(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing/app-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/app-routing/app-routing.module.ts ***!
  \***************************************************/
/*! exports provided: AppRoutingModule, routingComponents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "routingComponents", function() { return routingComponents; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../pages/home/home.component */ "./src/app/pages/home/home.component.ts");
/* harmony import */ var _pages_category_category_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../pages/category/category.component */ "./src/app/pages/category/category.component.ts");
/* harmony import */ var _pages_product_product_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../pages/product/product.component */ "./src/app/pages/product/product.component.ts");
/* harmony import */ var _pages_order_order_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../pages/order/order.component */ "./src/app/pages/order/order.component.ts");
/* harmony import */ var _pages_report_report_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../pages/report/report.component */ "./src/app/pages/report/report.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var routes = [
    { path: '', component: _pages_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: 'category', component: _pages_category_category_component__WEBPACK_IMPORTED_MODULE_4__["CategoryComponent"] },
    { path: 'product', component: _pages_product_product_component__WEBPACK_IMPORTED_MODULE_5__["ProductComponent"] },
    { path: 'order', component: _pages_order_order_component__WEBPACK_IMPORTED_MODULE_6__["OrderComponent"] },
    { path: 'report', component: _pages_report_report_component__WEBPACK_IMPORTED_MODULE_7__["ReportComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)
            ],
            exports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]
            ],
            declarations: []
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());

var routingComponents = [
    _pages_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
    _pages_category_category_component__WEBPACK_IMPORTED_MODULE_4__["CategoryComponent"],
    _pages_product_product_component__WEBPACK_IMPORTED_MODULE_5__["ProductComponent"],
    _pages_order_order_component__WEBPACK_IMPORTED_MODULE_6__["OrderComponent"],
    _pages_report_report_component__WEBPACK_IMPORTED_MODULE_7__["ReportComponent"]
];


/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container theme-showcase\">\n  <nav class=\"navbar navbar-default\">\n    <div class=\"container-fluid\">\n      <div class=\"navbar-header\">\n        <a class=\"navbar-brand\">\n          <img src=\"favicon.ico\" alt=\"\" class=\"logo\">\n        </a>\n        <span class=\"navbar-brand\">{{ title }}</span>\n      </div>\n      <ul class=\"nav navbar-nav\">\n        <li>\n          <a routerLink=\"\">Home</a>\n        </li>\n        <li>\n          <a routerLink=\"/category\">Category</a>\n        </li>\n        <li>\n          <a routerLink=\"/product\">Product</a>\n        </li>\n        <li>\n          <a routerLink=\"/order\">P.O.S</a>\n        </li>\n        <li>\n          <a routerLink=\"/report\">Report</a>\n        </li>\n      </ul>\n    </div>\n  </nav>\n  <router-outlet></router-outlet>\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Angular 6 Class';
        this.subTitle = 'One Week WorkShop';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _sample_table_table_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sample/table/table.component */ "./src/app/sample/table/table.component.ts");
/* harmony import */ var _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing/app-routing.module */ "./src/app/app-routing/app-routing.module.ts");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/order.service */ "./src/app/services/order.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _sample_table_table_component__WEBPACK_IMPORTED_MODULE_4__["TableComponent"],
                _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_5__["routingComponents"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _app_routing_app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"],
                ngx_bootstrap__WEBPACK_IMPORTED_MODULE_10__["ModalModule"].forRoot()
            ],
            providers: [_services_category_service__WEBPACK_IMPORTED_MODULE_6__["CategoryService"], _services_product_service__WEBPACK_IMPORTED_MODULE_7__["ProductService"], _services_order_service__WEBPACK_IMPORTED_MODULE_8__["OrderService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/base/config.ts":
/*!********************************!*\
  !*** ./src/app/base/config.ts ***!
  \********************************/
/*! exports provided: Config */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Config", function() { return Config; });
var Config = /** @class */ (function () {
    function Config() {
    }
    Config.Url = 'http://localhost:8100';
    return Config;
}());



/***/ }),

/***/ "./src/app/pages/category/category.component.css":
/*!*******************************************************!*\
  !*** ./src/app/pages/category/category.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/category/category.component.html":
/*!********************************************************!*\
  !*** ./src/app/pages/category/category.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"progress\" *ngIf=\"onProgress\">\n  <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\"\n    aria-valuemax=\"100\" style=\"width: 100%\">\n  </div>\n</div>\n<h3>{{ title }}</h3>\n<input type=\"button\" value=\"Create New\" class=\"btn btn-success\" (click)=\"onCreate(template)\">\n<table class=\"table\">\n  <thead>\n    <tr>\n      <th>Initial</th>\n      <th>Name</th>\n      <th>Active</th>\n      <th>Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let category of categories\">\n      <td>{{ category.initial }}</td>\n      <td>{{ category.name }}</td>\n      <td>\n        <input type=\"checkbox\" name=\"active\" class=\"form-check-input\" [(ngModel)]=\"category.active\" disabled=\"disabled\">\n      </td>\n      <td>\n        <div class=\"btn-group\" role=\"group\">\n          <button type=\"button\" class=\"btn btn-warning\" title=\"Edit\" (click)=\"onEdit(category._id, template)\">\n            <span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>\n          </button>\n          <button type=\"button\" class=\"btn btn-danger\" title=\"Delete\" (click)=\"onDelete(category._id, template)\">\n            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>\n          </button>\n        </div>\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n<ng-template #template>\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">{{ business }}</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"form-horizontal\">\n      <div class=\"form-group\">\n        <label for=\"initial\" class=\"control-label col-md-3\">Initial</label>\n        <div class=\"col-md-9\">\n          <input type=\"text\" name=\"intial\" class=\"form-control text-box single-line\" [(ngModel)]=\"category.initial\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"name\" class=\"control-label col-md-3\">Name</label>\n        <div class=\"col-md-9\">\n          <input type=\"text\" name=\"name\" class=\"form-control text-box single-line\" [(ngModel)]=\"category.name\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"name\" class=\"control-label col-md-3\">Active</label>\n        <div class=\"col-md-9\">\n          <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"category.active\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <div class=\"col-md-offset-2 col-md-10\">\n          <input type=\"button\" value=\"Cancel\" class=\"btn btn-default\" (click)=\"modalRef.hide()\">&nbsp;\n          <input type=\"button\" value=\"Save\" class=\"btn btn-success\" (click)=\"onSave()\">\n        </div>\n      </div>\n    </div>\n  </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/pages/category/category.component.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/category/category.component.ts ***!
  \******************************************************/
/*! exports provided: CategoryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryComponent", function() { return CategoryComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryComponent = /** @class */ (function () {
    function CategoryComponent(catSrv, modalService) {
        this.catSrv = catSrv;
        this.modalService = modalService;
        this.title = 'List of Category';
        this.business = 'Create';
        this.categories = [];
        this.category = { _id: '', initial: '', name: '', active: true };
        // showDetail = false;
        this.onProgress = true;
        this.disabled = false;
    }
    CategoryComponent.prototype.ngOnInit = function () {
        this.onGetAllCategories();
    };
    CategoryComponent.prototype.openModal = function (template) {
        this.modalRef = this.modalService.show(template);
    };
    CategoryComponent.prototype.onGetAllCategories = function () {
        var _this = this;
        this.onProgress = true;
        this.catSrv.getAllCategory()
            .subscribe(function (data) { return _this.categories = data; }, function (err) { return console.log(err); }, function () {
            _this.onProgress = false;
            console.log('Load categories is done!');
        });
    };
    CategoryComponent.prototype.onCreate = function (template) {
        this.category = { _id: '', initial: '', name: '' };
        this.disabled = false;
        this.modalRef = this.modalService.show(template);
    };
    CategoryComponent.prototype.onEdit = function (_id, template) {
        var _this = this;
        this.onProgress = true;
        this.disabled = false;
        this.business = 'Edit';
        this.catSrv.getCategoryById(_id)
            .subscribe(function (data) {
            _this.category = data;
            console.log(data);
        }, function (err) {
            alert(err);
            console.log(err);
        }, function () {
            _this.onProgress = false;
            _this.modalRef = _this.modalService.show(template);
        });
    };
    CategoryComponent.prototype.onDelete = function (_id, template) {
        var _this = this;
        this.disabled = true;
        this.business = 'Delete';
        this.catSrv.getCategoryById(_id)
            .subscribe(function (doc) {
            _this.category = doc;
            console.log(doc);
        }, function (err) { return alert(err); }, function () { return _this.modalRef = _this.modalService.show(template); });
    };
    CategoryComponent.prototype.onSave = function () {
        var _this = this;
        if (this.business === 'Delete') {
            console.log(this.category);
            this.catSrv.deleteCategory(this.category._id)
                .subscribe(function (doc) { return console.log(doc); }, function (err) {
                alert(err);
                console.log(err);
            }, function () {
                _this.onGetAllCategories();
                _this.modalRef.hide();
            });
        }
        else {
            if (this.category._id === '') {
                this.catSrv.postCategory(this.category)
                    .subscribe(function (doc) { return console.log(doc); }, function (err) {
                    alert(err);
                    console.log(err);
                }, function () {
                    _this.onGetAllCategories();
                    _this.modalRef.hide();
                });
            }
            else {
                this.catSrv.putCategory(this.category)
                    .subscribe(function (doc) { return console.log(doc); }, function (err) {
                    alert(err);
                    console.log(err);
                }, function () {
                    _this.onGetAllCategories();
                    _this.modalRef.hide();
                });
            }
        }
    };
    CategoryComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-category',
            template: __webpack_require__(/*! ./category.component.html */ "./src/app/pages/category/category.component.html"),
            styles: [__webpack_require__(/*! ./category.component.css */ "./src/app/pages/category/category.component.css")]
        }),
        __metadata("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_1__["CategoryService"], ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__["BsModalService"]])
    ], CategoryComponent);
    return CategoryComponent;
}());



/***/ }),

/***/ "./src/app/pages/home/home.component.css":
/*!***********************************************!*\
  !*** ./src/app/pages/home/home.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/home/home.component.html":
/*!************************************************!*\
  !*** ./src/app/pages/home/home.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h4>Welcome to Angular Web Application</h4>\n<p>One week training Back End API and Web Application</p>\n<p>Techology Node JS, Restify dan Angular 6</p>\n"

/***/ }),

/***/ "./src/app/pages/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/pages/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/pages/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/pages/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/pages/order/order.component.css":
/*!*************************************************!*\
  !*** ./src/app/pages/order/order.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/order/order.component.html":
/*!**************************************************!*\
  !*** ./src/app/pages/order/order.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>{{ title }}</h3>\n<div class=\"row\">\n  <div class=\"col-md-2\">\n    <input type=\"button\" value=\"Add Item\" class=\"btn btn-success\" (click)=\"onAdd(template)\">\n  </div>\n  <div class=\"col-md-2\">\n    <input type=\"button\" value=\"New Transaction\" class=\"btn btn-danger\" (click)=\"onNew()\">\n  </div>\n  <div class=\"col-md-2\">\n    <input type=\"button\" value=\"Payment\" class=\"btn btn-primary\" (click)=\"onPaymentPopup(templatePayment)\">\n  </div>\n  <div class=\"col-md-3\">\n    <input type=\"number\" name=\"grandTotal\" class=\"form-control text-box\" value=\"{{ getGrandTotal() }}\" readonly>\n  </div>\n</div>\n<table class=\"table\">\n  <thead>\n    <tr>\n      <th>Product</th>\n      <th>Price</th>\n      <th>Quantity</th>\n      <th>Amount</th>\n      <th>Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let order of orders; let i = index;\">\n      <td>\n        <input type=\"text\" name=\"productName\" class=\"form-control text-box single-line\" [(ngModel)]=\"order.productName\"\n          readonly>\n      </td>\n      <td>\n        <input type=\"number\" name=\"price\" class=\"form-control text-box single-line\" [(ngModel)]=\"order.price\" readonly>\n      </td>\n      <td>\n        <input type=\"number\" name=\"quantity\" class=\"form-control text-box single-line\" [(ngModel)]=\"order.quantity\">\n      </td>\n      <td>\n        <input type=\"number\" name=\"amount\" class=\"form-control text-box single-line\" [(ngModel)]=\"order.amount\" value=\"{{order.price * order.quantity}}\"\n          readonly>\n      </td>\n      <td>\n        <button type=\"button\" class=\"btn btn-danger\" title=\"Delete\" (click)=\"onRemove(i, order)\">\n          <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>\n        </button>\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n<ng-template #template>\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">Select Item</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <table class=\"table\">\n      <thead>\n        <tr>\n          <th>Select</th>\n          <th>Product</th>\n          <th>Price</th>\n        </tr>\n      </thead>\n      <tbody>\n        <tr *ngFor=\"let product of products\">\n          <td>\n            <button type=\"button\" class=\"btn btn-success\" title=\"Delete\" (click)=\"onInsert(product)\">\n              <span class=\"glyphicon glyphicon-shopping-cart\" aria-hidden=\"true\"></span>\n            </button>\n          </td>\n          <td>{{ product.name }}</td>\n          <td>{{ product.price }}</td>\n        </tr>\n      </tbody>\n    </table>\n  </div>\n</ng-template>\n\n<ng-template #templatePayment>\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">Payment</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"form-horizontal\">\n      <div class=\"form-group\">\n        <label for=\"price\" class=\"control-label col-md-3\">Pay</label>\n        <div class=\"col-md-9\">\n          <input type=\"number\" value=\"Payment\" name=\"payment\" class=\"form-control text-box\" [(ngModel)]=\"payment\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"amount\" class=\"control-label col-md-3\">Total</label>\n        <div class=\"col-md-9\">\n          <input type=\"number\" name=\"amount\" class=\"form-control text-box single-line\" [(ngModel)]=\"grandTotal\" value=\"\" readonly>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"change\" class=\"control-label col-md-3\">Change</label>\n        <div class=\"col-md-9\">\n          <input type=\"number\" name=\"change\" class=\"form-control text-box single-line\" value=\"{{payment - grandTotal}}\" readonly>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <div class=\"col-md-offset-2 col-md-10\">\n          <input type=\"button\" value=\"Payment\" class=\"btn btn-primary\" (click)=\"onPayment()\">\n        </div>\n      </div>\n    </div>\n  </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/pages/order/order.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/order/order.component.ts ***!
  \************************************************/
/*! exports provided: OrderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderComponent", function() { return OrderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/services/product.service.ts");
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/order.service */ "./src/app/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OrderComponent = /** @class */ (function () {
    function OrderComponent(modalService, prodSrv, orderSrv) {
        this.modalService = modalService;
        this.prodSrv = prodSrv;
        this.orderSrv = orderSrv;
        this.title = 'Point of Sale';
        this.products = [];
        this.orders = [];
        this.order = { productId: '', productName: '', quantity: 1, price: 0, amount: 0 };
        this.payment = 0;
        this.grandTotal = 0;
    }
    OrderComponent.prototype.ngOnInit = function () {
    };
    OrderComponent.prototype.onAdd = function (template) {
        var _this = this;
        this.prodSrv.getProductTrue()
            .subscribe(function (data) { return _this.products = data; }, function (err) { return alert(err); }, function () {
            return _this.modalRef = _this.modalService.show(template);
        });
    };
    OrderComponent.prototype.onInsert = function (product) {
        var newItem = {
            productId: product._id,
            productName: product.name,
            quantity: 1,
            price: product.price,
            amount: product.price
        };
        this.orders.push(newItem);
        this.modalRef.hide();
        // const sumValues = (orders) => Object.keys(orders).reduce((amount, value) => acc + ord[value], 0);
    };
    OrderComponent.prototype.onRemove = function (index, order) {
        if (confirm('Are you sure to delete ' + order.productName + '?')) {
            this.orders.splice(index, 1);
        }
    };
    OrderComponent.prototype.onPayment = function () {
        var _this = this;
        if (this.payment >= this.grandTotal) {
            var orders_1 = [];
            this.orders.forEach(function (order) {
                orders_1.push({
                    productId: order.productId,
                    quantity: order.quantity
                });
            });
            var headerDetail_1 = {
                payment: this.payment,
                orders: orders_1
            };
            this.orderSrv.postHeaderDetail(headerDetail_1)
                .subscribe(function (doc) { return console.log(doc); }, function (err) { return alert(err); }, function () {
                alert('Save Successful');
                _this.orders = [];
                _this.payment = 0;
                console.log(headerDetail_1);
                _this.modalRef.hide();
            });
        }
        else {
            alert('Payment must be greater or equals to Grand Total!');
        }
    };
    OrderComponent.prototype.getGrandTotal = function () {
        var grTotal = 0;
        this.orders.forEach(function (order) {
            grTotal = parseFloat(grTotal) + parseFloat(order.price) * parseFloat(order.quantity);
        });
        this.grandTotal = grTotal;
        return grTotal;
    };
    OrderComponent.prototype.onNew = function () {
        if (confirm('Are you sure to remova all order?')) {
            this.orders = [];
            this.payment = 0;
            this.grandTotal = 0;
        }
    };
    OrderComponent.prototype.onPaymentPopup = function (template) {
        this.modalRef = this.modalService.show(template);
    };
    OrderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-order',
            template: __webpack_require__(/*! ./order.component.html */ "./src/app/pages/order/order.component.html"),
            styles: [__webpack_require__(/*! ./order.component.css */ "./src/app/pages/order/order.component.css")]
        }),
        __metadata("design:paramtypes", [ngx_bootstrap__WEBPACK_IMPORTED_MODULE_1__["BsModalService"], _services_product_service__WEBPACK_IMPORTED_MODULE_2__["ProductService"], _services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "./src/app/pages/product/product.component.css":
/*!*****************************************************!*\
  !*** ./src/app/pages/product/product.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/product/product.component.html":
/*!******************************************************!*\
  !*** ./src/app/pages/product/product.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"progress\" *ngIf=\"onProgress\">\n  <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\"\n    aria-valuemax=\"100\" style=\"width: 100%\">\n  </div>\n</div>\n<h3>{{ title }}</h3>\n<input type=\"button\" value=\"Create New\" class=\"btn btn-success\" (click)=\"onCreate(template)\">\n<table class=\"table\">\n  <thead>\n    <tr>\n      <th>Initial</th>\n      <th>Category</th>\n      <th>Name</th>\n      <th>Description</th>\n      <th>Price</th>\n      <th>Active</th>\n      <th>Action</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let product of products\">\n      <td>{{ product.initial }}</td>\n      <td>{{ product.category.initial }}</td>\n      <td>{{ product.name }}</td>\n      <td>{{ product.desc }}</td>\n      <td>{{ product.price }}</td>\n      <td>\n        <input type=\"checkbox\" name=\"active\" class=\"form-check-input\" [(ngModel)]=\"product.active\" disabled=\"disabled\">\n      </td>\n      <td>\n        <div class=\"btn-group\" role=\"group\">\n          <button type=\"button\" class=\"btn btn-warning\" title=\"Edit\" (click)=\"onEdit(product._id, template)\">\n            <span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>\n          </button>\n          <button type=\"button\" class=\"btn btn-danger\" title=\"Delete\" (click)=\"onDelete(product._id, template)\">\n            <span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>\n          </button>\n        </div>\n      </td>\n    </tr>\n  </tbody>\n</table>\n\n<ng-template #template>\n  <div class=\"modal-header\">\n    <h4 class=\"modal-title pull-left\">{{ business }}</h4>\n    <button type=\"button\" class=\"close pull-right\" aria-label=\"Close\" (click)=\"modalRef.hide()\">\n      <span aria-hidden=\"true\">&times;</span>\n    </button>\n  </div>\n  <div class=\"modal-body\">\n    <div class=\"form-horizontal\">\n      <div class=\"form-group\">\n        <label for=\"categoryId\" class=\"control-label col-md-3\">Category</label>\n        <div class=\"col-md-9\">\n          <select class=\"form-control\" name=\"categoryId\" [(ngModel)]=\"product.categoryId\" [disabled]=\"disabled\">\n            <option value=\"\">Select Category</option>\n            <option *ngFor=\"let category of categories\" value=\"{{ category._id }}\">{{ category.name }}</option>\n          </select>\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"initial\" class=\"control-label col-md-3\">Initial</label>\n        <div class=\"col-md-9\">\n          <input type=\"text\" name=\"initial\" class=\"form-control text-box single-line\" [(ngModel)]=\"product.initial\"\n            [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"name\" class=\"control-label col-md-3\">Name</label>\n        <div class=\"col-md-9\">\n          <input type=\"text\" name=\"name\" class=\"form-control text-box single-line\" [(ngModel)]=\"product.name\"\n            [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"desc\" class=\"control-label col-md-3\">Description</label>\n        <div class=\"col-md-9\">\n          <input type=\"text\" name=\"desc\" class=\"form-control text-box multi-line\" [(ngModel)]=\"product.desc\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"price\" class=\"control-label col-md-3\">Price</label>\n        <div class=\"col-md-9\">\n          <input type=\"number\" name=\"price\" class=\"form-control text-box single-line\" [(ngModel)]=\"product.price\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <label for=\"name\" class=\"control-label col-md-3\">Active</label>\n        <div class=\"col-md-9\">\n          <input type=\"checkbox\" name=\"active\" [(ngModel)]=\"product.active\" [disabled]=\"disabled\">\n        </div>\n      </div>\n      <div class=\"form-group\">\n        <div class=\"col-md-offset-2 col-md-10\">\n          <input type=\"button\" value=\"Cancel\" class=\"btn btn-default\" (click)=\"modalRef.hide()\">&nbsp;\n          <input type=\"button\" value=\"Save\" class=\"btn btn-success\" (click)=\"onSave()\">\n        </div>\n      </div>\n    </div>\n  </div>\n</ng-template>"

/***/ }),

/***/ "./src/app/pages/product/product.component.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/product/product.component.ts ***!
  \****************************************************/
/*! exports provided: ProductComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductComponent", function() { return ProductComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/category.service */ "./src/app/services/category.service.ts");
/* harmony import */ var ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-bootstrap */ "./node_modules/ngx-bootstrap/index.js");
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/product.service */ "./src/app/services/product.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ProductComponent = /** @class */ (function () {
    function ProductComponent(catSrv, prodSrv, modalService) {
        this.catSrv = catSrv;
        this.prodSrv = prodSrv;
        this.modalService = modalService;
        this.title = 'List of Product';
        this.products = [];
        this.product = { _id: '', categoryId: '', initial: '', name: '', desc: '', price: 0, active: true };
        this.categories = [];
        this.onProgress = true;
        this.disabled = false;
        this.business = 'Create';
    }
    ProductComponent.prototype.ngOnInit = function () {
        this.onGetAllProducts();
        this.onGetAllCategory();
    };
    ProductComponent.prototype.onGetAllProducts = function () {
        var _this = this;
        this.onProgress = true;
        this.prodSrv.getAllProduct()
            .subscribe(function (data) { return _this.products = data; }, function (err) { return console.log(err); }, function () {
            _this.onProgress = false;
            console.log('Load productss is done!');
        });
    };
    ProductComponent.prototype.onGetAllCategory = function () {
        var _this = this;
        this.catSrv.getCategoryTrue()
            .subscribe(function (data) { return _this.categories = data; }, function (err) { return console.log(err); }, function () { });
    };
    ProductComponent.prototype.onCreate = function (template) {
        this.product = { _id: '', categoryId: '', initial: '', name: '', desc: '', price: 0, active: true };
        this.disabled = false;
        this.modalRef = this.modalService.show(template);
    };
    ProductComponent.prototype.onEdit = function (_id, template) {
        var _this = this;
        this.onProgress = true;
        this.disabled = false;
        this.business = 'Edit';
        this.prodSrv.getProductById(_id)
            .subscribe(function (data) {
            _this.product = data;
            console.log(data);
        }, function (err) {
            alert(err);
            console.log(err);
        }, function () {
            _this.onProgress = false;
            _this.modalRef = _this.modalService.show(template);
        });
    };
    ProductComponent.prototype.onDelete = function (_id, template) {
        var _this = this;
        this.disabled = true;
        this.business = 'Delete';
        this.prodSrv.getProductById(_id)
            .subscribe(function (doc) {
            _this.product = doc;
            console.log(doc);
        }, function (err) { return alert(err); }, function () { return _this.modalRef = _this.modalService.show(template); });
    };
    ProductComponent.prototype.onSave = function () {
        var _this = this;
        if (this.business === 'Delete') {
            this.prodSrv.deleteProduct(this.product._id)
                .subscribe(function (doc) { return console.log(doc); }, function (err) {
                alert(err);
                console.log(err);
            }, function () {
                _this.onGetAllProducts();
                _this.modalRef.hide();
            });
        }
        else {
            if (this.product._id === '') {
                this.prodSrv.postProduct(this.product)
                    .subscribe(function (doc) { return console.log(doc); }, function (err) {
                    alert(err);
                    console.log(err);
                }, function () {
                    _this.onGetAllProducts();
                    _this.modalRef.hide();
                });
            }
            else {
                this.prodSrv.putProduct(this.product)
                    .subscribe(function (doc) { return console.log(doc); }, function (err) {
                    alert(err);
                    console.log(err);
                }, function () {
                    _this.onGetAllProducts();
                    _this.modalRef.hide();
                });
            }
        }
    };
    ProductComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-product',
            template: __webpack_require__(/*! ./product.component.html */ "./src/app/pages/product/product.component.html"),
            styles: [__webpack_require__(/*! ./product.component.css */ "./src/app/pages/product/product.component.css")]
        }),
        __metadata("design:paramtypes", [_services_category_service__WEBPACK_IMPORTED_MODULE_1__["CategoryService"],
            _services_product_service__WEBPACK_IMPORTED_MODULE_3__["ProductService"],
            ngx_bootstrap__WEBPACK_IMPORTED_MODULE_2__["BsModalService"]])
    ], ProductComponent);
    return ProductComponent;
}());



/***/ }),

/***/ "./src/app/pages/report/report.component.css":
/*!***************************************************!*\
  !*** ./src/app/pages/report/report.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/pages/report/report.component.html":
/*!****************************************************!*\
  !*** ./src/app/pages/report/report.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h3>{{title}}</h3>\n<table class=\"table\">\n  <thead>\n    <tr>\n      <th>Date</th>\n      <th>Reference</th>\n      <th>Product</th>\n      <th>Price</th>\n      <th>Quantity</th>\n      <th>Amount</th>\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let hd of headerDetail\">\n      <td>{{ hd.createDate | date:'dd MMM yy HH:mm'}}</td>\n      <td>{{ hd.reference }}</td>\n      <td>{{ hd.oDetail.productName }}</td>\n      <td>{{ hd.oDetail.price }}</td>\n      <td>{{ hd.oDetail.quantity }}</td>\n      <td>{{ hd.oDetail.price * hd.oDetail.quantity }}</td>\n    </tr>\n  </tbody>\n</table>"

/***/ }),

/***/ "./src/app/pages/report/report.component.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/report/report.component.ts ***!
  \**************************************************/
/*! exports provided: ReportComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportComponent", function() { return ReportComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_report_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/report.service */ "./src/app/services/report.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ReportComponent = /** @class */ (function () {
    function ReportComponent(rptSrv) {
        this.rptSrv = rptSrv;
        this.title = 'Report of Point of Sale';
        this.headerDetail = [];
    }
    ReportComponent.prototype.ngOnInit = function () {
        this.getAllReport();
    };
    ReportComponent.prototype.getAllReport = function () {
        var _this = this;
        this.rptSrv.getOrderReport()
            .subscribe(function (data) { return _this.headerDetail = data; }, function (err) { return console.log(err); }, function () { return console.log('Report is loaded!'); });
    };
    ReportComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-report',
            template: __webpack_require__(/*! ./report.component.html */ "./src/app/pages/report/report.component.html"),
            styles: [__webpack_require__(/*! ./report.component.css */ "./src/app/pages/report/report.component.css")]
        }),
        __metadata("design:paramtypes", [_services_report_service__WEBPACK_IMPORTED_MODULE_1__["ReportService"]])
    ], ReportComponent);
    return ReportComponent;
}());



/***/ }),

/***/ "./src/app/sample/table/table.component.css":
/*!**************************************************!*\
  !*** ./src/app/sample/table/table.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/sample/table/table.component.html":
/*!***************************************************!*\
  !*** ./src/app/sample/table/table.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"!showDetail\">\n  <h3>List of Category</h3>\n  <input type=\"button\" value=\"Create\" title=\"Create\" (click)=\"onCreate()\" />\n  <table>\n    <thead>\n      <tr>\n        <th>No.</th>\n        <th>Initial</th>\n        <th>Name</th>\n        <th>Action</th>\n      </tr>\n    </thead>\n    <tbody>\n      <tr *ngFor=\"let category of categories; let i = index;\" id=\"tr-{{i + 1}}\">\n        <td>{{ i + 1 }}</td>\n        <td class=\"initial\">{{ category.initial }}</td>\n        <td>{{ category.name }}</td>\n        <td>\n          <input type=\"button\" title=\"Edit\" value=\"Edit\" (click)=\"onEdit(category._id)\" />\n          <input type=\"button\" title=\"Delete\" value=\"Delete\" (click)=\"onDelete(category._id)\" />\n        </td>\n      </tr>\n    </tbody>\n  </table>\n</div>\n\n<div *ngIf=\"showDetail\">\n  <h4>{{ business }}</h4>\n  <form>\n    Initial:<br>\n    <input type=\"text\" [(ngModel)]=\"category.initial\" name=\"initial\"><br>\n    Name:<br>\n    <input type=\"text\" [(ngModel)]=\"category.name\" name=\"name\"><br>\n    <input type=\"submit\" value=\"Submit\"/>\n    <input type=\"button\" value=\"Cancel\" (click)=\"onCancel()\"/>\n  </form>\n</div>\n\n<span>{{ category | json }}</span>"

/***/ }),

/***/ "./src/app/sample/table/table.component.ts":
/*!*************************************************!*\
  !*** ./src/app/sample/table/table.component.ts ***!
  \*************************************************/
/*! exports provided: TableComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableComponent", function() { return TableComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TableComponent = /** @class */ (function () {
    function TableComponent() {
        this.categories = [
            { _id: 1, initial: 'VEG', name: 'Vegetable' },
            { _id: 2, initial: 'FRU', name: 'Fruit' },
            { _id: 3, initial: 'MLK', name: 'Milk' },
            { _id: 4, initial: 'KMJ', name: 'Kemeja' },
            { _id: 5, initial: 'RIC', name: 'Rice' }
        ];
        this.category = {
            _id: 0,
            initial: '',
            name: ''
        };
        this.showDetail = false;
        this.business = 'Create';
    }
    TableComponent.prototype.ngOnInit = function () {
    };
    TableComponent.prototype.onCreate = function () {
        this.category = {
            _id: 0,
            initial: '',
            name: ''
        };
        // console.log(this.category);
        this.business = 'Create';
        this.showDetail = true;
    };
    TableComponent.prototype.onEdit = function (_id) {
        var cat = this.categories.find(function (o) { return o._id === _id; });
        this.category = {
            _id: cat._id,
            initial: cat.initial,
            name: cat.name
        };
        // console.log(this.category);
        this.business = 'Edit';
        this.showDetail = true;
    };
    TableComponent.prototype.onDelete = function (_id) {
        this.category = this.categories.find(function (o) { return o._id === _id; });
        // console.log(this.category);
    };
    TableComponent.prototype.onCancel = function () {
        this.showDetail = false;
    };
    TableComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-table',
            template: __webpack_require__(/*! ./table.component.html */ "./src/app/sample/table/table.component.html"),
            styles: [__webpack_require__(/*! ./table.component.css */ "./src/app/sample/table/table.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TableComponent);
    return TableComponent;
}());



/***/ }),

/***/ "./src/app/services/category.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/category.service.ts ***!
  \**********************************************/
/*! exports provided: CategoryService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CategoryService", function() { return CategoryService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _base_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base/config */ "./src/app/base/config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CategoryService = /** @class */ (function () {
    function CategoryService(http) {
        this.http = http;
        this.categories = [];
    }
    CategoryService.prototype.printConsole = function (arg) {
        console.log(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + 'Category service respon this : ' + arg);
    };
    CategoryService.prototype.getAllCategory = function () {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/category');
    };
    CategoryService.prototype.getCategoryById = function (_id) {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/category/' + _id);
    };
    CategoryService.prototype.postCategory = function (category) {
        var newCat = {
            initial: category.initial,
            name: category.name,
            active: category.active
        };
        return this.http.post(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/category', newCat);
    };
    CategoryService.prototype.putCategory = function (category) {
        var newCat = {
            initial: category.initial,
            name: category.name,
            active: category.active
        };
        return this.http.put(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/category/' + category._id, newCat);
    };
    CategoryService.prototype.deleteCategory = function (_id) {
        console.log('deleteCategory:' + _id);
        return this.http.delete(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/category/' + _id);
    };
    CategoryService.prototype.getCategoryTrue = function () {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/cattrue');
    };
    CategoryService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], CategoryService);
    return CategoryService;
}());



/***/ }),

/***/ "./src/app/services/order.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/order.service.ts ***!
  \*******************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _base_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base/config */ "./src/app/base/config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
    }
    OrderService.prototype.postHeaderDetail = function (headerDetail) {
        return this.http.post(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/headdet', headerDetail);
    };
    OrderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], OrderService);
    return OrderService;
}());



/***/ }),

/***/ "./src/app/services/product.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/product.service.ts ***!
  \*********************************************/
/*! exports provided: ProductService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductService", function() { return ProductService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _base_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base/config */ "./src/app/base/config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductService = /** @class */ (function () {
    function ProductService(http) {
        this.http = http;
    }
    ProductService.prototype.printConsole = function (arg) {
        console.log(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + 'Product service respon this : ' + arg);
    };
    ProductService.prototype.getAllProduct = function () {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/product');
    };
    ProductService.prototype.getProductById = function (_id) {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/product/' + _id);
    };
    ProductService.prototype.postProduct = function (product) {
        var newProd = {
            categoryId: product.categoryId,
            initial: product.initial,
            name: product.name,
            desc: product.desc,
            price: product.price,
            active: product.active
        };
        return this.http.post(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/product', newProd);
    };
    ProductService.prototype.putProduct = function (product) {
        var newProd = {
            categoryId: product.categoryId,
            initial: product.initial,
            name: product.name,
            desc: product.desc,
            price: product.price,
            active: product.active
        };
        return this.http.put(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/product/' + product._id, newProd);
    };
    ProductService.prototype.deleteProduct = function (_id) {
        return this.http.delete(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/product/' + _id);
    };
    ProductService.prototype.getProductTrue = function () {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/prodtrue');
    };
    ProductService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ProductService);
    return ProductService;
}());



/***/ }),

/***/ "./src/app/services/report.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/report.service.ts ***!
  \********************************************/
/*! exports provided: ReportService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReportService", function() { return ReportService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _base_config__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base/config */ "./src/app/base/config.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ReportService = /** @class */ (function () {
    function ReportService(http) {
        this.http = http;
    }
    ReportService.prototype.getOrderReport = function () {
        return this.http.get(_base_config__WEBPACK_IMPORTED_MODULE_2__["Config"].Url + '/api/orderReport');
    };
    ReportService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ReportService);
    return ReportService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\WorkShop\MenKeu\web6\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map