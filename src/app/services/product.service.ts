import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from './../base/config';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: any = [];

  constructor(private http: HttpClient) { }

  getAllProducts() {
    return this.http.get(Config.Url + '/api/products', this.setOptions());
  }

  getProdyctById(_id){
    return this.http.get(Config.Url + '/api/product/' + _id, this.setOptions());
  }

  findProducts(query){
    return this.http.post(Config.Url + '/api/findproduct', query, this.setOptions());
  }

  postProduct(product){
    const newProduct: any = {
      initial: product.initial,
      name: product.name,
      description: product.description,
      price: product.price,
      active: product.active,
      categoryId: product.categoryId
    };

    return this.http.post(Config.Url + '/api/product', newProduct, this.setOptions());
  }

  putProduct(product){
    const newProduct: any = {
      initial: product.initial,
      name: product.name,
      description: product.description,
      price: product.price,
      active: product.active,
      categoryId: product.categoryId
    };

    return this.http.put(Config.Url + '/api/product/' + product._id, newProduct, this.setOptions());
  }
  
  deleteProduct(_id){
    return this.http.delete(Config.Url + '/api/product/' + _id, this.setOptions());
  }

  private setOptions(){
    const accessToken = localStorage.getItem('accessToken');
    return {headers: new HttpHeaders().set('Authorization', `Bearer ${accessToken}`)}
  }
}
