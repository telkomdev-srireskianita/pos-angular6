import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  postOrder(orders){
      return this.http.post(Config.Url + '/api/order', orders, this.setOptions());
  }

  private setOptions(){
    const accessToken = localStorage.getItem('accessToken');
    return {headers: new HttpHeaders().set('Authorization', `Bearer ${accessToken}`)}
  }
}
