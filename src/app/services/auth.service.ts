import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public isAuthenticated() {
    return localStorage.getItem('accessToken');
  }

  private setSession(authResult){
    const expiresAt = moment().add(authResult.expiresIn, 'second');

    localStorage.setItem('accessToken', authResult.accessToken);
    localStorage.setItem('expiredAt', JSON.stringify(expiresAt.valueOf()));
  }
  
  logout(){
    localStorage.removeItem('accessToken');
    localStorage.removeItem('expiredAt');
  }

  public isLoggedIn(){
    return moment().isBefore(this.getExpiration());
  }

  isLoggedOut(){
    return !this.isLoggedIn();
  }

  getExpiration(){
    const expiration = localStorage.getItem('expiredAt');
    const expiredAt = JSON.parse(expiration);
    return moment(expiredAt);
  }
}
