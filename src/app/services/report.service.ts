import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) {}

  getAllReports(){
    return this.http.get(Config.Url + '/api/orders', this.setOptions());
  }

  private setOptions(){
    const accessToken = localStorage.getItem('accessToken');
    return {headers: new HttpHeaders().set('Authorization', `Bearer ${accessToken}`)}
  }
}
