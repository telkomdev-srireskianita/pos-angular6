import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../base/config';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) {}

  login(query){
    return this.http.post(Config.Url + '/api/login', query);
  }
}
