import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Config } from '../base/config';
import { SessionService } from './session.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categories: any = [];
  constructor(
    private http: HttpClient,
    private session: SessionService,
    ) { }

  getAllCategory() {
    return this.http.get(Config.Url + '/api/categories', this.setOptions());
  }

  getAllActiveCategory(){
    return this.http.get(Config.Url + '/api/categoriesActive', this.setOptions());
  }

  getCategoryById(_id) {
    return this.http.get(Config.Url + '/api/category/' + _id, this.setOptions());
  }

  findCategories(query){
    return this.http.post(Config.Url + '/api/findcategory', query, this.setOptions());
  }

  postCategory(category) {
    const newCat: any = {
      initial: category.initial,
      name: category.name,
      active: category.active
    };
    return this.http.post(Config.Url + '/api/category', newCat, this.setOptions());
  }

  putCategory(category) {
    const newCat: any = {
      initial: category.initial,
      name: category.name,
      active: category.active
    };
    return this.http.put(Config.Url + '/api/category/' + category._id, newCat, this.setOptions());
  }

  deleteCategory(_id) {
    return this.http.delete(Config.Url + '/api/category/' + _id, this.setOptions());
  }

  getCategoryTrue() {
    return this.http.get(Config.Url + '/api/cattrue', this.setOptions());
  }
  
  private setOptions(){
    const accessToken = localStorage.getItem('accessToken');
    return {headers: new HttpHeaders().set('Authorization', `Bearer ${accessToken}`)}
  }
}
