import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  categories = [
    { _id: 1, initial: 'VEG', name: 'Vegetable' },
    { _id: 2, initial: 'FRU', name: 'Fruit' },
    { _id: 3, initial: 'MLK', name: 'Milk' },
    { _id: 4, initial: 'KMJ', name: 'Kemeja' },
    { _id: 5, initial: 'RIC', name: 'Rice' }
  ];
  category = {
    _id: 0,
    initial: '',
    name: ''
  };
  showDetail = false;
  business = 'Create';
  constructor() { }

  ngOnInit() {
  }

  onCreate() {
    this.category = {
      _id: 0,
      initial: '',
      name: ''
    };
    // console.log(this.category);
    this.business = 'Create';
    this.showDetail = true;
  }

  onEdit(_id) {
    const cat = this.categories.find(o => o._id === _id);
    this.category = {
      _id : cat._id,
      initial : cat.initial,
      name : cat.name
    };
    // console.log(this.category);
    this.business = 'Edit';
    this.showDetail = true;
  }

  onDelete(_id) {
    this.category = this.categories.find(o => o._id === _id);
    // console.log(this.category);
  }

  onCancel() {
    this.showDetail = false;
  }
}
