import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { CategoryComponent } from '../pages/category/category.component';
import { ProductComponent } from '../pages/product/product.component';
import { OrderComponent } from '../pages/order/order.component';
import { ReportComponent } from '../pages/report/report.component';
import { LoginComponent } from '../pages/login/login.component';
import { GuardService } from '../services/guard.service';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'product', component: ProductComponent },
  { path: 'order', component: OrderComponent },
  { path: 'report', component: ReportComponent },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }

export const routingComponents = [
  HomeComponent,
  CategoryComponent,
  ProductComponent,
  OrderComponent,
  ReportComponent,
  LoginComponent
];
