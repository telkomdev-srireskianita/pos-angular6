import { Component, OnInit, TemplateRef } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ProductService } from 'src/app/services/product.service';
import { OrderService } from 'src/app/services/order.service';

@Component({
    selector: 'app-order',
    templateUrl: './order.component.html',
    styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
    title = 'Point of Sale';
    message: string = 'Another Message';
    onProgress = false;
    categories: any = [];
    products: any = { _id: '', initial: '', name: '', active: true, price: '' };
    modalRef: BsModalRef;
    business = 'Select Item';
    objProducts: any = [];
    totals = 0;
    values = '';
    totalAmount = 0;
    totalChange: any;
    totalPay = 0;
    data = 0;
    orderProducts: any = [];
    isAddItem = false;
    isPayment = false;
    refNumber: any;
    constructor(private catServ: CategoryService,
        private modalService: BsModalService,
        private proServ: ProductService,
        private ordServ: OrderService) { }

    ngOnInit() {
        this.orderProducts;
    }

    openModal(template: TemplateRef<any>) {
        this.modalRef = this.modalService.show(template);
    }

    ngOnChange(event: any, index, price) {
        this.orderProducts[index]['total'] = event.target.value * price;
        const datas = this.orderProducts;
        this.totalAmount = 0;
        datas.map(d => {
            if (d.total) {
                this.totalAmount = d.total + this.totalAmount;
            }
        });
    }

    onDelete(index) {
        if (confirm('Are you sure to delete ' + this.orderProducts[index].name + '?')) {
            this.products.push(this.orderProducts[index]);
            this.totalAmount = this.totalAmount - this.orderProducts[index].total;
            this.orderProducts.splice(index, 1);
        } 
    }

    addItem(template: TemplateRef<any>) {
        this.onGetAllProducts();
        this.isPayment = false;
        this.isAddItem = true;
        this.modalRef = this.modalService.show(template);
    }

    newTransaction() {
        if (confirm('Are you sure to remove all order?')) {
            this.orderProducts = [];
            this.totalAmount = 0;
            this.products = [];
        }
    }

    checkOrderProduct(message) {
        if (this.orderProducts.length === 0) {
            alert(message);
        }
    }

    setFormat(price){
        var formater = new Intl.NumberFormat('id-ID')
        return formater.format(price)
      }

    searchProduct(event: any) {
        const query = { name: event.target.value }
        this.findProducts(query);
    }

    addProduct(product) {
        product['qty'] = 1;
        product.total = product.price;
        this.totalAmount = this.totalAmount + product.price;
        this.orderProducts.push(product);
        const index = this.products.indexOf(product);
        this.products.splice(index, 1);
        this.modalRef.hide();
    }

    openPayment(template: TemplateRef<any>) {
        if (!(this.orderProducts.length === 0)) {
            this.business = "Payment";
            this.isAddItem = false;
            this.totalChange = this.totalPay - this.totalAmount;
            this.isPayment = true;
            this.modalRef = this.modalService.show(template);
        }

        this.checkOrderProduct('Belum ada Product yang dipilih');
    }

    addPayment(event: any) {
        this.totalChange = event.target.value - this.totalAmount;
    }

    payNow() {
        if (this.totalChange > 0) {
            alert('Uang Anda kelebihan ' + this.totalChange);
        } else if (this.totalChange < 0) {
            alert('Uang Anda kurang ' + (this.totalChange * -1));
        } else {
            const newOrder = {
                total: this.totalAmount,
            }
            let product = [];
            this.orderProducts.map(p => {
                let newProduct = {
                    _id: p._id,
                    name: p.name,
                    price: p.price,
                    qty: p.qty,
                    total: p.total
                }

                product.push(newProduct);
            })

            newOrder['products'] = product;
            this.ordServ.postOrder(newOrder)
                .subscribe((order) => {
                    this.totalPay = 0;
                    this.refNumber = order['data'].ref
                }, (err) => {
                    alert(err);
                    console.log(err);
                },
                    () => {
                        this.orderProducts = [];
                        this.totalAmount = 0;
                    })
        }
    }

    closePayment() {
        this.refNumber = '';
        this.totalAmount = 0;
        this.modalRef.hide();
    }

    onGetAllProducts() {
        if (this.products.length === 0) {
            this.onProgress = true;
            this.proServ.getAllProducts()
                .subscribe((data) => this.products = data['data'],
                    (err) => console.log(err),
                    () => {
                        this.onProgress = false;
                    })
        }
    }

    findProducts(query) {
        this.proServ.findProducts(query)
            .subscribe((data) => this.products = data['data'],
                (err) => console.log(err));
      }

    onGetAllCategories() {
        this.onProgress = true;
        this.catServ.getAllCategory()
            .subscribe((data) => this.categories = data['data'],
                (err) => console.log(err),
                () => {
                    this.onProgress = false;
                })
    }
}
;