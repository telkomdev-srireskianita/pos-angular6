import { Component, OnInit } from '@angular/core';
import { ReportService } from 'src/app/services/report.service';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  title = 'Report of Point of Sale';
  orders: any = [];
  onProgress = false;
  grantTotal: any = 0;
  products: any;
  totalQty: any = 0;
  constructor(private repServ: ReportService, private proServ: ProductService) { }

  ngOnInit() {
    this.onGetAllProduct();
    this.onGetAllOrder();
    this.totals();
  }

  formatUang(totalPrice) {
    var formater = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    })

    return formater.format(totalPrice)
  }

  setFormat(price) {
    var formater = new Intl.NumberFormat('id-ID')
    return formater.format(price)
  }

  formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];
    var newDate = new Date(date);
    var day = newDate.getDate();
    var monthIndex = newDate.getMonth();
    var year = newDate.getFullYear();
    var hours = newDate.getHours();
    var minutes = newDate.getMinutes();
    var second = newDate.getSeconds();
    return day + ' ' + monthNames[monthIndex] + ' ' + year + '; ' + hours + ':' + minutes + ':' + second;
  }

  onGetAllOrder() {
    let totalPrice = 0;
    let totalQty = 0;
    this.onProgress = true;
    this.repServ.getAllReports()
      .subscribe((data) => {
        this.orders = data['data'];
        data['data'].map(order => {
          totalPrice = totalPrice + order.total;

          order.products.map(product => {
            totalQty = totalQty + parseInt(product.qty);
          });
        });

        this.totalQty = totalQty;
        this.grantTotal = this.formatUang(totalPrice);
      },
        (err) => console.log(err),
        () => {
          this.onProgress = false;
        })
  }

  getProductPrice(_id) {
    if (this.products) {
      let product = this.products.find(product => product._id === _id);
      return product.price;
    }
  }

  onGetAllProduct() {
    this.proServ.getAllProducts()
      .subscribe((product) => this.products = product['data'],
        (err) => console.log(err));
  }

  totals() {



  }


}
