import { Component, OnInit, TemplateRef } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})

export class CategoryComponent implements OnInit {
  title = 'List of Category';
  business = 'Create';
  categories: any = [];
  category: any = { _id: '', initial: '', name: '', active: true };
  onProgress = true;
  disabled = false;
  modalRef: BsModalRef;

  constructor(private catSrv: CategoryService, private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetAllCategories();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onGetAllCategories() {
    this.onProgress = true;
    this.catSrv.getAllCategory()
      .subscribe((data) => this.categories = data['data'],
        (err) => console.log(err),
        () => {
          this.onProgress = false;
        });
  }

  onCreate(template: TemplateRef<any>) {
    this.business = 'Create';
    this.category = { _id: '', initial: '', name: '', active: true };
    this.disabled = false;
    this.modalRef = this.modalService.show(template);
  }

  onEdit(_id, template: TemplateRef<any>) {
    this.onProgress = true;
    this.disabled = false;
    this.business = 'Edit';
    this.catSrv.getCategoryById(_id)
      .subscribe((data) => {
        this.category = data['data'];
        console.log(data);
      },
        (err) => {
          alert(err);
          console.log(err);
        },
        () => {
          this.onProgress = false;
          this.modalRef = this.modalService.show(template);
        });
  }

  onDelete(_id, template: TemplateRef<any>) {
    this.disabled = true;
    this.business = 'Delete';
    this.catSrv.getCategoryById(_id)
      .subscribe((doc) => {
        this.category = doc['data'];
      },
        (err) => alert(err),
        () => this.modalRef = this.modalService.show(template));
  }

  searchCategory(event: any){
    const query = { name: event.target.value }
    this.findCategories(query);
  }

  findCategories(query) {
    this.catSrv.findCategories(query)
        .subscribe((data) => this.categories = data['data'],
            (err) => console.log(err));
  } 
  
  onSave() {
    if (this.business === 'Delete') {
      console.log(this.category);
      this.catSrv.deleteCategory(this.category._id)
        .subscribe((doc) => console.log(doc),
          (err) => {
            console.log(err);
          },
          () => {
            this.onGetAllCategories();
            this.modalRef.hide();
          });
    } else {
      if (this.category._id === '') {
        this.catSrv.postCategory(this.category)
          .subscribe((doc) => console.log(doc),
            (err) => {
              console.log(err);
            },
            () => {
              this.onGetAllCategories();
              this.modalRef.hide();
            });
      } else {
        this.catSrv.putCategory(this.category)
          .subscribe((doc) => console.log(doc),
            (err) => {
              console.log(err);
            },
            () => {
              this.onGetAllCategories();
              this.modalRef.hide();
            });
      }
    }
  }
}
