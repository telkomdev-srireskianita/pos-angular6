import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData: any = [];

  public frm: FormGroup;
  
  public isBusy = false;
  public hasFaild = false;
  public showInputErrors = false;

  constructor(
    private logServ: LoginService,
    private auth: AuthService,
    private router: Router
    ) {}

  ngOnInit() {
    console.log(this.loginData);
  }

  public loginUser(event){
    event.preventDefault();
    const target = event.target;
    const username = target.querySelector("#username").value;
    const password = target.querySelector("#password").value;

    this.isBusy = true;
    this.hasFaild = false;
    
    const userData = {
      "username": username,
      "password": password
    }

    this.logServ.login(userData)
        .subscribe((response) => { 
          const data = response['data'];
          localStorage.setItem('accessToken', data.accessToken);
          console.log(data);
        },
        (error) => {
          this.isBusy = false;
          this.hasFaild = true;
        }
        );
  }
}
