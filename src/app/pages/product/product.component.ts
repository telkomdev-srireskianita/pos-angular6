import { Component, OnInit, TemplateRef } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {
  title = 'List of Product';
  business = 'Create';
  products: any = [];
  categories: any = [];
  product: { _id: '', initial: '', name: '', description: '', active: true, categoryId: '' };
  onProgress = true;
  disabled = false;
  isDelete = false;
  modalRef: BsModalRef;
  constructor(private proSrv: ProductService,
    private modalService: BsModalService,
    private catSrv: CategoryService) { };

  ngOnInit() {
    this.getlAllCategories();
    this.onGetAllProducts();
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  onGetAllProducts() {
    this.onProgress = true;
    this.proSrv.getAllProducts()
      .subscribe((data) => this.products = data['data'],
        (err) => console.log(err),
        () => {
          this.onProgress = false;
        });
  }

  getlAllCategories() {
    this.catSrv.getAllActiveCategory()
      .subscribe((data) => this.categories = data['data'],
        (err) => console.log(err));
  }

  onCreate(template: TemplateRef<any>) {
    this.business = 'Create';
    this.product = { _id: '', initial: '', name: '', description: '', active: true, categoryId: '' };
    this.getlAllCategories();
    this.disabled = false;
    this.modalRef = this.modalService.show(template);
  }

  onDelete(_id, template: TemplateRef<any>) {
    this.disabled = true;
    this.business = 'Delete';
    this.proSrv.getProdyctById(_id)
      .subscribe((doc) => {
        this.getlAllCategories();
        this.product = doc['data'];
      },
        (err) => alert(err),
        () => this.modalRef = this.modalService.show(template));
  }

  onEdit(_id, template: TemplateRef<any>) {
    this.onProgress = true;
    this.disabled = false;
    this.business = 'Edit';
    this.getlAllCategories();
    this.proSrv.getProdyctById(_id)
      .subscribe((data) => {
        this.product = data['data'];
      }, (err) => {
        console.log(err);
      },
        () => {
          this.onProgress = false;
          this.modalRef = this.modalService.show(template);
        });
  }

  setFormat(price) {
    var formater = new Intl.NumberFormat('id-ID')
    return formater.format(price)
  }


  searchProduct(event: any) {
    const query = { name: event.target.value }
    this.findProducts(query);
  }

  findProducts(query) {
    this.proSrv.findProducts(query)
      .subscribe((data) => this.products = data['data'],
        (err) => console.log(err));
  }

  getCategorybyId(categoryId) {
    if (this.categories.length !== 0) {
      let category = this.categories.find(cat => cat._id === categoryId);
      return category.name;
    }
  }

  onSave() {
    if (this.business === 'Delete') {
      this.proSrv.deleteProduct(this.product._id)
        .subscribe((doc) => console.log(doc),
          (err) => {
            console.log(err);
          },
          () => {
            this.onGetAllProducts();
            this.modalRef.hide();
          })
    } else {
      if (this.product._id === '') {
        this.proSrv.postProduct(this.product).subscribe((doc) => console.log(doc),
          (err) => {
            console.log(err);
          },
          () => {
            this.onGetAllProducts();
            this.modalRef.hide();
          })
      } else {
        this.proSrv.putProduct(this.product).subscribe((doc) => console.log(doc),
          (err) => {
            console.log(err);
          },
          () => {
            this.onGetAllProducts();
            this.modalRef.hide();
          })
      }
    }
  }
}
