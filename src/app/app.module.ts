import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { TableComponent } from './sample/table/table.component';
import { AppRoutingModule, routingComponents } from './app-routing/app-routing.module';

import { CategoryService } from './services/category.service';
import { ProductService } from './services/product.service';
import { OrderService } from './services/order.service';
import { LoginService } from './services/login.service';


import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    routingComponents,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [CategoryService, ProductService, OrderService, LoginService],
  bootstrap: [AppComponent]
})


export class AppModule { }


